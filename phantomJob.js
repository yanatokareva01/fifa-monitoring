/**
 * Created by Yana Tokareva on 28.05.2018.
 */

const phantom = require('phantom');
const cheerio = require('cheerio');
const rp = require('request-promise');
const logger = require('./logger');

const sourcePage = 'https://tickets.fifa.com/API/WCachedL1/en/BasicCodes/GetBasicCodesAvailavilityDemmand?currencyId=USD';
const phoneNumber = '89818266125';
const username = 'yanatokareva01@gmail.com';
const password = 'EjZY3iK60AeXXMbDmJO5rXP4I0k0';
const email = 'xjoexx@gmail.com';
const api_key = 'ccb4fb93b7418117a32adc93429d3884-b6183ad4-0072c9a7';
const mailgun = require('mailgun-js')({ apiKey: api_key });
let lastSmsTime = null;

let _page;

module.exports = {
	doJob
};

function doJob(phantom) {
	phantom.createPage().then(function(page){
		_page = page;

		return _page.open(sourcePage);
	}).then(() => {
		return _page.property('content')
	}).then((content) => {
		const $ = cheerio.load(content);
		const basicCodes = JSON.parse($('pre')[0].children[0].data).Data;

		return basicCodes.Availability.reduce(function(res, item) {
			const product = getProductByProductId(item.p, basicCodes);
			const stadium = getStadiaByStadiumId(product.MatchStadium, basicCodes);
			const category = getCategoryByCode(item.c, basicCodes);

			if (item.a && item.c === 17) {
				const key = item.p;
				if (!(key in res)) {
					const productCategories = {
						productId: item.p,
						productName: product.ProductPublicName,
						productOrder: product.SO,
						productTypeCode: product.ProductTypeCode,
						matchDate: product.MatchDate,
						matches: product.Matches,
						rounds: product.Rounds ? product.Rounds.split(',') : [],
						stadiumName: stadium ? stadium.StadiumName : '',
						categories: {}
					};
					productCategories.categories[item.c] = { categoryName: category.CategoryName, value: item.a };

					res[key] = productCategories;
				} else {
					if (res[key].categories[item.c]) {
						res[key].categories[item.c].value += item.a;
					} else {
						res[key].categories[item.c] = { categoryName: category.CategoryName, value: item.a };
					}
				}
			}
			return res;
		}, {});
	}).then((result) => {
		let report = '';
		for (let game in result) {
			report += `${result[game].productName} :: ${result[game].matchDate}\n`;

			for (let category in result[game].categories) {
				report += `${result[game].categories[category].categoryName} :: ${result[game].categories[category].value} билета\n`;
			}

			report += '\n\n';
		}

		return report;
	}).then((report) => {
		if (report) {
			logger.info('есть доступные билеты');
			logger.info(report);
		} else {
			logger.info('нет доступных билетов')
		}
		if (report && report.length && (!lastSmsTime || (new Date() - lastSmsTime > 1000 * 60 * 30))) {
			rp.get(`https://gate.smsaero.ru/send/?user=${username}&password=${password}&to=${phoneNumber}&text=${encodeURI('Новые билеты!')}&from=SMS Aero`).then((res) => {
				logger.info(res);
				lastSmsTime = new Date();
			});
		} else {
			logger.info(`смска не отправлена. Время последней смски ${lastSmsTime}`);
		}
	}).then(() => {
		_page.close();
	}).catch((err) => {
		logger.info(err);
	});
}

function getCategoryByCode(categoryId, basicCodes) {
	return getItemInBasicCodes(categoryId, 'CATEGORIES', 'CategoryId', basicCodes);
}

function getProductByProductId(productId, basicCodes) {
	return getItemInBasicCodes(productId, 'PRODUCTIMT', 'ProductId', basicCodes);
}

function getStadiaByStadiumId(stadiumId, basicCodes) {
	return getItemInBasicCodes(stadiumId, 'VENUES', 'StadiumCityId', basicCodes);
}

function getItemInBasicCodes(itemId, basicCodesList, basicCodesItemKey, basicCodes) {
	let itemFound = null;
	for (let i = 0, len = basicCodes[basicCodesList].length; i < len; i++) {
		if (itemId === basicCodes[basicCodesList][i][basicCodesItemKey]) {
			itemFound = basicCodes[basicCodesList][i];
			break;
		}
	}
	return itemFound;
}