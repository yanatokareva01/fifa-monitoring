/**
 * Created by Yana Tokareva on 28.05.2018.
 */
const phantom = require('phantom');
const cron = require('cron');
const job = require('./phantomJob');
const logger = require('./logger');
let _ph;

const fifaJob = new cron.CronJob({
	cronTime: '*/3 * * * *',
	onTick: function() {
		job.doJob(_ph);
	},
	start: false
});

phantom.create().then(function(ph){
	_ph = ph;
	logger.info('job started');
	fifaJob.start();
});

process.on('close', function() {
	fifaJob.end();
	_ph.exit();
	logger.info('job finished');
});
