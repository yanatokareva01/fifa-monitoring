/**
 * Created by Yana Tokareva on 29.05.2018.
 */
const winston = require('winston');

const logger = winston.createLogger({
	level: 'info',
	transports: [
		new winston.transports.Console({})
	],
	format: winston.format.combine(winston.format.timestamp(), winston.format.splat(), winston.format.simple())
});

module.exports = logger;